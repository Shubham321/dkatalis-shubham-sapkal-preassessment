package dkatalis;

import java.util.Arrays;

public class Que18 {
	static boolean areAnagram(String st1, String st2) 
    { 
		char [] str1 = st1.toCharArray();
		char [] str2 = st2.toCharArray();
        
        int n1 = str1.length; 
        int n2 = str2.length; 
  
       
        if (n1 != n2) 
            return false; 
  
       
        Arrays.sort(str1); 
        Arrays.sort(str2); 
  
        
        for (int i = 0; i < n1; i++) 
            if (str1[i] != str2[i]) 
                return false; 
  
        return true; 
    } 
  
   
    public static void main(String args[]) 
    { 
       String s1 = "ABCD";
       String s2 = "DCBA"; 
        if (areAnagram(s1, s2)) 
            System.out.println("The two strings are"
                               + " anagram of each other"); 
        else
            System.out.println("The two strings are not"
                               + " anagram of each other"); 
    } 
}