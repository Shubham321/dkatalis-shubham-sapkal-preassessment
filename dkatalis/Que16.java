package dkatalis;

import java.util.Scanner;

public class Que16 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int times = scan.nextInt();
		scan.nextLine();
		String UDs = scan.nextLine();
		int[] levels = new int[times];
		int up = 0, down = 0, count = 0;
		for (int i = 0; i < times; i++) {
			if (UDs.charAt(i) == 'U') {
				up++;
			} else {
				down++;
			}
			levels[i] = up - down;

		}
		for (int i = 0; i < times; i++) {
			if (levels[i] < 0) {
				for (int ii = i; ii < times; ii++) {
					if (levels[ii] == 0) {
						count++;
						i = ii;
						break;
					}
				}
			}
		}
		System.out.println(count);
	}
}
